from MyCollection import MyCollection

collection = MyCollection()
collection.add_item(1)
collection.add_item(10)
collection.add_item(100)

for i in collection.get_reverse_iterator():
    print(i)

print("Straight traversal:")
print("\n".join(collection))
print("")

print("Reverse traversal:")
print("\n".join(collection.get_reverse_iterator()), end="")
from typing import Any, List, Iterable

from MyIterator import MyIterator

class MyCollection(Iterable):

    def __init__(self, collection: List[Any] = []):
        self._collection = collection

    def __iter__(self):
        return MyIterator(self._collection)

    def get_reverse_iterator(self):
        return MyIterator(self._collection, True)

    def add_item(self, item: Any):
        self._collection.append(item)
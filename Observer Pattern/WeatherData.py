from MobileDisplay import MobileDisplay
from Observeable import Observeable
from TabletDisplay import TabletDisplay


class WeatherData(Observeable):

    def __init__(self):
        super().__init__()
        self._tempreture = 0
        self._humidity = 0
        self._pressure = 0
    
    @property
    def tempreture(self):
        return self._tempreture
    
    @tempreture.setter
    def tempreture(self, value):
        self._tempreture = value
        self.measurementsChanged()
    
    @property
    def humidity(self):
        return self._humidity
    
    @humidity.setter
    def humidity(self, value):
        self._humidity = value
        
    @property
    def pressure(self):
        return self._pressure
    
    @pressure.setter
    def pressure(self, value):
        self._pressure = value

    def measurementsChanged(self):
        self.notify()
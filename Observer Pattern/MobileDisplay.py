from Display import Display


class MobileDisplay(Display):
    
    def update(self, weatherData):
        print("Show Data In Mobile Format :\n temp = {} \n humidity = {} \n pressure = {} ".format(weatherData.tempreture, weatherData.humidity, weatherData.pressure))
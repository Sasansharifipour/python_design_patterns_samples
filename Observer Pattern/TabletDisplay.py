from Display import Display


class TabletDisplay(Display):
    
    def update(self, weatherData):
        print("Show Data In Tablet Format :\t temp = {} \t humidity = {} \t pressure = {} ".format(weatherData.tempreture, weatherData.humidity, weatherData.pressure))
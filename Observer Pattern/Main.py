from MobileDisplay import MobileDisplay
from TabletDisplay import TabletDisplay
from WeatherData import WeatherData


weatherData = WeatherData()

mobile_display = MobileDisplay()
tablet_display = TabletDisplay()

weatherData.attach(mobile_display)
weatherData.attach(tablet_display)

weatherData.tempreture = 10
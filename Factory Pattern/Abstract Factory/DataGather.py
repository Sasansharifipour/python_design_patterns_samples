from abc import ABC, abstractmethod


class DataGather(ABC):

    @abstractmethod
    def gather(self):
        pass
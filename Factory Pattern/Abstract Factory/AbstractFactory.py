from abc import ABC, abstractmethod

class AbstractFactory(ABC):
    
    @abstractmethod
    def Create(self):
        pass
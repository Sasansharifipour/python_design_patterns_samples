from DatabaseLogger import DatabaseLogger
from FileLogger import FileLogger
from AbstractFactory import AbstractFactory

class LoggerFactory(AbstractFactory):

    def __init__(self):
        self.path = {
        "File": FileLogger,
        "Database": DatabaseLogger,
    }

    def Create(self, location):
        return self.path[location]()


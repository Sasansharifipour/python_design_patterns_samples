from LoggerFactory import LoggerFactory
from DataGatherFactory import DataGatherFactory

class CreateData:
    
    def __init__(self) -> None:
        self.log_location = "File"
        self.data_gether = "Web"

    def create(self):
        logger_factory = LoggerFactory()
        data_logger = logger_factory.Create(self.log_location)

        gather_factory = DataGatherFactory()
        data_gather = gather_factory.Create(self.data_gether)

        data_logger.log()
        data_gather.gather()



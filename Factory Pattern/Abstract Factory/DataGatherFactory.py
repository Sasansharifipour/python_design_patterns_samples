from WebGather import WebGather
from EternetGather import EternetGather
from AbstractFactory import AbstractFactory

class DataGatherFactory(AbstractFactory):

    def __init__(self):
        self.path = {
        "Web": WebGather,
        "Eternet": EternetGather,
    }

    def Create(self, location):
        return self.path[location]()
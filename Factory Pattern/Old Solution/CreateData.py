from DatabaseLogger import DatabaseLogger
from FileLogger import FileLogger


class CreateData:
    
    def __init__(self) -> None:
        self.log_location = "Database"

    def create(self):

        if (self.log_location == "File"):
            data_logger = FileLogger()
        elif (self.log_location == "Database"):
            data_logger = DatabaseLogger()
        
        data_logger.log()



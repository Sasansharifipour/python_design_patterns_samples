from abc import ABC, abstractmethod


class DataLogger(ABC):
    
    @abstractmethod
    def log(self):
        pass
    
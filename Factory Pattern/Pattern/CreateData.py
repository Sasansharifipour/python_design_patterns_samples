from Factory import Factory


class CreateData:
    
    def __init__(self) -> None:
        self.log_location = "File"

    def create(self):
        factory = Factory()
        data_logger = factory.Create(self.log_location)
        
        data_logger.log()



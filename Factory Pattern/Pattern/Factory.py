from DatabaseLogger import DatabaseLogger
from FileLogger import FileLogger

class Factory:

    def __init__(self):
        self.path = {
        "File": FileLogger,
        "Database": DatabaseLogger,
    }

    def Create(self, location):
        return self.path[location]()


from Duck import Duck
from FlyNoWay import FlyNoWay
from JigJigSoung import JigJigSound

class RubberDuck(Duck):

    def __init__(self, name) -> None:
        super().__init__(name)
        self.flyBehavior = FlyNoWay()
        self.soundBehavior = JigJigSound()
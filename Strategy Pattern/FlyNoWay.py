from FlyBehavior import FlyBehavior


class FlyNoWay(FlyBehavior):
    def fly(self):
        print("Can not Fly")
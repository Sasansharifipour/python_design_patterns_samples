from abc import ABC, abstractmethod


class SoundBehavior(ABC):
    @abstractmethod
    def quack(self):
        pass
from Duck import Duck
from FlyWithWings import FlyWithWings
from JigJigSoung import JigJigSound


class SpecialDuck(Duck):
    def __init__(self, name) -> None:
        super().__init__(name)
        self.flyBehavior = FlyWithWings()
        self.soundBehavior = JigJigSound()
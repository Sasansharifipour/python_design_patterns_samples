from Duck import Duck
from FlyWithWings import FlyWithWings
from QuackSound import QuackSound


class RedheadDuck(Duck):
    
    def __init__(self, name) -> None:
        super().__init__(name)
        self.flyBehavior = FlyWithWings()
        self.soundBehavior = QuackSound()
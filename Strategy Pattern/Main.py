from MallardDuck import MallardDuck
from RedheadDuck import RedheadDuck
from RubberDuck import RubberDuck
from SpecialDuck import SpecialDuck

ducks = []

ducks.append(SpecialDuck("Special"))
ducks.append(RedheadDuck("RedHead"))
ducks.append(RubberDuck("Rubber"))

for duck in ducks:
    print('-----------------------------')
    duck.display()
    duck.swim()
    duck.quack()
    duck.fly()
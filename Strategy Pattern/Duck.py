from FlyBehavior import FlyBehavior
from SoundBehavior import SoundBehavior


class Duck:
    def __init__(self, name) -> None:
        self.name = name
    
    @property
    def flyBehavior(self) -> FlyBehavior:
        return self._flyBehavior
    
    @flyBehavior.setter
    def flyBehavior(self, flyBehavior: FlyBehavior):
        self._flyBehavior = flyBehavior

    @property
    def soundBehavior(self) -> SoundBehavior:
        return self._soundBehavior
    
    @soundBehavior.setter
    def soundBehavior(self, soundBehavior: SoundBehavior):
        self._soundBehavior = soundBehavior
    
    def display(self):
        print("My name is : "+ self.name)

    def swim(self):
        print("Swim")

    def quack(self):
        self._soundBehavior.quack()
    
    def fly(self):
        self._flyBehavior.fly()
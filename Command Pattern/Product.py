class Product:

    def __init__(self, name, price) -> None:
        self.name = name
        self.price = price
    
    def increasePrice(self, amount) :
        self.price += amount
        print("The price for the {} has been increased by {}$.".format(self.name, amount))
    
    def decreasePrice(self, amount):
        if (amount < self.price):
            self.price -= amount
            print("The price for the {} has been decreased by {}$.".format(self.name, amount))
            return True
        else:
            print("The price for the {} has not changed.".format(self.name))
            return False

    def ToString(self):
        return "Current price for the {} product is {}$.".format(self.name, self.price)

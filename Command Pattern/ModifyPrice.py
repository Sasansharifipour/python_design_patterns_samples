class ModifyPrice:

    def __init__(self) -> None:
        self._commands = []
    
    def SetCommand(self, command):
        self._command = command

    def Invoke(self):
        self._commands.append(self._command)
        self._command.Execute()
        
    def UndoActions(self):
        for command in reversed(self._commands):
            command.Undo()
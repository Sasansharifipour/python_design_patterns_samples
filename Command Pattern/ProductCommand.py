from Command import Command
from Product import Product


class ProductCommand(Command):

    def __init__(self, product, priceAction, amount) -> None:
        self._product = product
        self._priceAction = priceAction
        self._amount = amount

    @property
    def IsCommandExecuted(self):
        return self._isCommandExecuted
    
    def Execute(self):
        if (self._priceAction == "Increase"):
            self._product.increasePrice(self._amount)
            self._isCommandExecuted = True
        else:
            self._isCommandExecuted  = self._product.decreasePrice(self._amount)
        
    def Undo(self):
        if (self.IsCommandExecuted == False):
            return
        
        if (self._priceAction == "Increase"):
            self._product.decreasePrice(self._amount)
        else:
            self._product.increasePrice(self._amount)
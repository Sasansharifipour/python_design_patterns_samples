from ModifyPrice import ModifyPrice
from Product import Product
from ProductCommand import ProductCommand


modifyPrice = ModifyPrice()
product = Product("Phone", 500)

cmd1 = ProductCommand(product, "Decrease", product.price * 20 / 100)

modifyPrice.SetCommand(cmd1)
modifyPrice.Invoke()

cmd2 = ProductCommand(product, "Increase", product.price * 9 / 100)
modifyPrice.SetCommand(cmd2)
modifyPrice.Invoke()

print(product.ToString())

modifyPrice.UndoActions()
print(product.ToString())
from Book import Book
from BookManager import BookManager
from User import User
from UserManager import UserManager


class FacadeBookLoan:

    def __init__(self) -> None:
        self._bookManager = BookManager()
        self._userManager = UserManager()
    
    def loan_book(self, bookname, username):
        book : Book = self._bookManager.get_book(bookname)
        user : User = self._userManager.get_user(username)

        if (book.IsLoanable() == False):
            print("This book is not loanable!!!")
        else:
            if (book._count <= 0):
                print("There is no book left")
            else:
                if (len(user.get_loaned_books_list()) >= 2):
                    print("This User can not loan book")
                else:
                    book._count -= 1
                    user._books.append(book)
                    print("Book Loaned")
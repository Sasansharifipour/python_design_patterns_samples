class User:

    def __init__(self, name) -> None:
        self._name = name
        self._books = []
    
    @property
    def name(self):
        return self._name
    
    def get_loaned_books_list(self):
        return self._books
class Book:

    def __init__(self, name, count, is_loanable) -> None:
        self._name = name
        self._count = count
        self._is_loanable = is_loanable
    
    def IsLoanable(self):
        return self._is_loanable
    
    @property
    def name(self):
        return self._name
    
    
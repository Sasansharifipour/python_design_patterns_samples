from Book import Book


class BookManager:

    def __init__(self) -> None:
        self._books = []
        self._books.append(Book("C#", 3, True))
        self._books.append(Book("C++", 2, True))
        self._books.append(Book("History", 1, False))
    
    def get_book(self, name):
        for book in self._books:
            if (book.name  == name):
                return book
    

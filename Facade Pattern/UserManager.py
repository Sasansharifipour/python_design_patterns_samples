from User import User


class UserManager:

    def __init__(self) -> None:
        self.Users = []

        self.Users.append(User("Sasan"))
        self.Users.append(User("Ali"))
    
    def get_user(self, name):
        for user in self.Users:
            if (user.name == name):
                return user
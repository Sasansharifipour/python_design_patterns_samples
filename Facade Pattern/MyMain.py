from FacadeBookLoan import FacadeBookLoan


facadebookloan = FacadeBookLoan()

facadebookloan.loan_book("C#", "Sasan")
facadebookloan.loan_book("C++", "Sasan")
facadebookloan.loan_book("C#", "Sasan")
facadebookloan.loan_book("C++", "Ali")
facadebookloan.loan_book("C++", "Ali")
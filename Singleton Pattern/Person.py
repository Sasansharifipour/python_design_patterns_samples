class Person:

    def __init__(self) -> None:
        self.__name = "Sasan"

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = value
        
    def to_string(self):
        print("Name : {}".format(self.__name))
class Singleton:
    _instance = None

    def __new__(cls, *args, **kwargs):
        
        if (cls._instance is None):
            cls._instance = super().__new__(cls)
        
        return cls._instance
    
    def __init__(self, name):
        self.name = name
    
    def to_string(self):
        print("Name : {}".format(self.name))
from CondimentDecorator import CondimentDecorator

class Mocha(CondimentDecorator):
    def __init__(self, beverage) -> None:
        super().__init__(beverage)
        self._beverage = beverage
    
    def getDescription(self):
        return self._beverage.getDescription() + ", Mocha"
    
    def cost(self):
        return self._beverage.cost() + 2
from Beverage import Beverage


class Espresso(Beverage):

    def __init__(self) -> None:
        super().__init__()
        self.description = 'Espresso'

    def getDescription(self):
        return super().getDescription()

    def cost(self):
        return 10.0
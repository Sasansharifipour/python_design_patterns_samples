from abc import ABC, abstractmethod


class Beverage(ABC):

    def __init__(self) -> None:
        super().__init__()
        
    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, value):
        self._description = value
    
    def getDescription(self):
        return self.description

    @abstractmethod
    def cost():
        return
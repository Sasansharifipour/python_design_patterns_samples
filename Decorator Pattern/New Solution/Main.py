from Mocha import Mocha
from Espresso import Espresso
from Sugar import Sugar

espresso = Espresso()

print(espresso.getDescription() + '\n' + str(espresso.cost()))

espresso = Mocha(espresso)

print(espresso.getDescription() + '\n' + str(espresso.cost()))

espresso = Mocha(espresso)

print(espresso.getDescription() + '\n' + str(espresso.cost()))

espresso = Sugar(espresso)

print(espresso.getDescription() + '\n' + str(espresso.cost()))
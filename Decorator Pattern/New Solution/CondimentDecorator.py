from abc import abstractmethod


class CondimentDecorator:

    def __init__(self, beverage) -> None:
        self._beverage = beverage

    @abstractmethod
    def getDescription(self):
        return
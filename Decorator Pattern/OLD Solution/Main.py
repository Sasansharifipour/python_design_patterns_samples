from DarkRoast import DarkRoast
from Espresso import Espresso
from EspressoWithSugar import EspressoWithSugar


espresso = Espresso()
darkRoast = DarkRoast()
espressoWithSugar = EspressoWithSugar()

espresso.setSugar(True)

print(espresso.cost())
print(darkRoast.cost())
print(espressoWithSugar.cost())
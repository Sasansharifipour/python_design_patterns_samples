from Beverage import Beverage


class DarkRoast(Beverage):

    def __init__(self) -> None:
        super().__init__()

        self.description = 'Dark Roast'
    
    def cost(self):
        return 9.0
    
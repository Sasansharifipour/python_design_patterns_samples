from Beverage import Beverage


class Espresso(Beverage):

    def __init__(self) -> None:
        super().__init__()
        self._sugar = False
        self.description = 'Espresso'
    
    def setSugar(self, value):
        self._sugar = value
    
    def hasSugar(self):
        return self._sugar

    def cost(self):
        if (self.hasSugar()):
            return 10.0 + 2.0
        else:
            return 10.0
def sum(*args):
    sum = 0

    for num in args:
        sum += num

    return sum

def info(**kwargs):

    if ("name" in kwargs.keys()):
        print("Name : " + kwargs["name"])
    else:
        print("There is no name in info")

info(name="Sasan", age=23, sex="male")
info(age=23, sex="male", skincolor="black")

print(sum(10, 12, 15, 18))

print(sum(10, 12, 15, 18, 20))
from Beverage import Beverage


class HouseBlend(Beverage):

    def __init__(self) -> None:
        super().__init__()

        self.description = 'House Blend'
    
    def cost(self):
        return 11.5
    
from Beverage import Beverage


class EspressoWithSugar(Beverage):

    def __init__(self) -> None:
        super().__init__()

        self.description = 'Espresso With Sugar'
    
    def cost(self):
        return 12.0
from Person import Person


def copy_person(person):
    person2 = Person("")

    person2.name = person.name

    return person2

person1 = Person("Sasan")
person1.set_age(10)

person2 = person1.clone()

person1.to_string()
person2.to_string()

person2.set_age(20)

person1.to_string()
person2.to_string()
from Iprototype import IPrototype


class Person(IPrototype):

    def __init__(self, name) -> None:
        self.name = name
        self.__age = 0
    
    def set_age(self, age):
        self.__age = age
    
    def to_string(self):
        print("Name : {} & age : {}".format(self.name, self.__age))

    def clone(self):
        obj = Person(self.name)
        obj.__age = self.__age

        return obj
from ConcreteState import StandbyState
from Context import Context


context = Context(StandbyState())
print('--------------------------------------------')
context.PressPlay()
print('--------------------------------------------')
context.PressPlay()
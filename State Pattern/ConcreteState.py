from State import State


class StandbyState(State):
    def PressPlay(self) -> None:
        print("StandbyState handle press play button")
        print("StandbyState wants to change the state of the context.")
        self.context.transition_to(PlayingState())

class PlayingState(State):
    def PressPlay(self) -> None:
        print("PlayingState handle press standby button")
        print("PlayingState wants to change the state of the context.")
        self.context.transition_to(StandbyState())
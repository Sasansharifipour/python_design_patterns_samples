from Device import Device


class TV(Device):

    def __init__(self) -> None:
        self._enabled = False
        self._volume = 0
        self._channel = 1

    def isEnabled(self):
        return self._enabled

    def enable(self):
        print("TV Power on")
        self._enabled = True

    def disable(self):
        print("TV Power off")
        self._enabled = False

    def getVolume(self):
        return self._volume

    def setVolume(self, volume):
        print("TV Volume set to : {}".format(volume))
        self._volume = volume
    
    def getChannel(self):
        return self._channel

    def setChannel(self, channel):
        self._channel = channel
from abc import ABC, abstractmethod


class Device(ABC):

    @abstractmethod
    def isEnabled(self):
        pass

    @abstractmethod
    def enable(self):
        pass

    @abstractmethod
    def disable(self):
        pass

    @abstractmethod
    def getVolume(self):
        pass

    @abstractmethod
    def setVolume(self, volume):
        pass
    
    @abstractmethod
    def getChannel(self):
        pass

    @abstractmethod
    def setChannel(self, channel):
        pass
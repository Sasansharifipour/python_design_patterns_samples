class Remote:

    def __init__(self, device) -> None:
        self._device = device
    
    def togglePower(self):
        if (self._device.isEnabled()):
            self._device.disable()
        else:
            self._device.enable()
    
    def volumeUp(self):
        self._device.setVolume(self._device.getVolume() + 1)

    def volumeDown(self):
        self._device.setVolume(self._device.getVolume() - 1)
    
    def channelUp(self):
        self._device.setChannel(self._device.getChannel() + 1)

    def channelDown(self):
        self._device.setChannel(self._device.getChannel() - 1)
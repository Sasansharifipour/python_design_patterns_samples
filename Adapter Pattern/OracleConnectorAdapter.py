from DatabaseConnector import DatabaseConnector
from OracleConnector import OracleConnector


class OracleConnectorAdapter(DatabaseConnector, OracleConnector):
    
    def open(self):
        return self.open_conncetion()
    
    def run_query(self, query):
        return self.execute_query(query)
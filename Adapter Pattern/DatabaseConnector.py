
from abc import ABC, abstractmethod


class DatabaseConnector(ABC):

    @abstractmethod
    def open(self):
        pass
    
    @abstractmethod
    def run_query(self, query):
        pass
from xml.etree.ElementInclude import default_loader
from DatabaseConnector import DatabaseConnector
from MSSQLConnector import MSSQLConnector


class MSSQLConnectorAdapter(DatabaseConnector, MSSQLConnector):

    def open(self):
        return self.open_database()
    
    def run_query(self, query):
        return self.run_command(query)
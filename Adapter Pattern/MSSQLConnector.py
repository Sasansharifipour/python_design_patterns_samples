class MSSQLConnector:
    def open_database(self):
        print("Open Connection to MSSQL database.")

    def run_command(self, query):
        print("Run command : '{}' on MSSQL database.".format(query))